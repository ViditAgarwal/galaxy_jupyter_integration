
# coding: utf-8

# In[81]:


import nbtools
import csv


# In[2]:


@nbtools.build_ui
def say_hello(whom='World'):
    """Say hello to the world or whomever."""
    print('Hello ' + whom)


# In[3]:


@nbtools.build_ui
def say_hello(whom='World'):
    """Say hello to the world or whomever."""
    print('Hello ' + whom)


# @nbtools.build_ui(name="custom name", description="custom description")
# def example_function(arg1, arg2):
#     return (arg1, arg2)

# In[2]:


@nbtools.build_ui(name="custom name", description="custom description")
def example_function(arg1, arg2):
    return (arg1, arg2)


# In[11]:


@nbtools.build_ui(name="Elucidata", description="Elucidata is working on Galaxy-Jupyter notebook integration")
def example_function(arg1, arg2):
    return (arg1, arg2)


# In[ ]:


@nbtools.build.ui(name="File Upload" , description='Upload the user csv file')
def uploader_function() :


# In[9]:


import nbtools

# Instantiate the NBTool object with necessary metadata
hello_tool = nbtools.NBTool(id='hello_tool', name='Hello World Tool', origin='Notebook')


# In[10]:


hello_tool_load = lambda: None                    # Called when registered


# In[11]:



hello_tool.render = lambda: print('Hello World')  # Called when the tool is selected

# Register the tool with the tool manager
nbtools.register(hello_tool)


# In[12]:


@nbtools.build_ui(name="custom name", description="custom description")
def example_function(arg1, arg2):
    return (arg1, arg2)


# In[13]:


@nbtools.build_ui
def say_hello(whom='World'):
    """Say hello to the world or whomever."""
    print('Hello ' + whom)


# In[17]:


@nbtools.build_ui(parameters={
    "param_1": {
        "type": "file",
        "kinds": ["gct", "odf"]
    }
})
def example_function(param_1, param_2):
    print("Hello World")


# In[18]:


"""@nbtools.build_ui(parameters={
    "param_1": {
        "type": "file",
        "kinds": ["gct", "odf"]
    }
})
def example_function(param_1, param_2):
    print()
    """


# In[ ]:


@nbtools.build_ui(parameters={
    "File": {
        "type": "file",
        "kinds": ["gct", "odf" , "csv" , "pdf"]
    }
})
def file_uploader(param_1):
    print("Hello World")


# In[79]:


sum =0 
@nbtools.build_ui(parameters={
    "param_1": {
        "type": "file",
        "kinds": ["gct", "odf" , "csv" , "pdf"]
    }
})
def file_uploader(param_1):
    print(param_1)
    print(type(param_1))
    f= open(r"/home/vidit/Desktop/Galaxy-Jupyter/" +param_1 , "r")
    print(f)
    contents = f.read()
    print(contents)
    with open(r"/home/vidit/Desktop/Galaxy-Jupyter/" +param_1) as fin:
        for line in fin :
            print(int(line[0]) + int(line[2]))
            global sum 
            sum += (int(line[0]) + int(line[2])) 
    print(sum)


# In[37]:


f= open(r"/home/vidit/Desktop/Galaxy-Jupyter/test.csv" , "r")


# In[38]:


print(f.read())


# In[40]:


print("a"+"b")


# In[80]:


sum =0 
@nbtools.build_ui(parameters={
    "param_1": {
        "type": "file",
        "kinds": ["gct", "odf" , "csv" , "pdf"]
    }
})
def file_uploader(param_1):
    print(param_1)
    print(type(param_1))
    f= open(r"/home/vidit/Desktop/Galaxy-Jupyter/" +param_1 , "r")
    print(f)
    contents = f.read()
    print(contents)
    with open(r"/home/vidit/Desktop/Galaxy-Jupyter/" +param_1) as fin:
        for line in fin :
            print(int(line[0]) + int(line[2]))
            global sum 
            sum += (int(line[0]) + int(line[2])) 
    print(sum)

