from bioblend import galaxy

def main() :

	galaxy_instance = galaxy.GalaxyInstance(url='https://usegalaxy.org', key='c2f7f155404714560bae599e53fa9fe2')

	# create  a new history from the api

	history_dict = galaxy_instance.histories.create_history('testing-larger-file')

	history_id = history_dict['id']
	print(history_dict['id'])

	histories = galaxy_instance.histories.get_histories()

	#wf = galaxy_instance.workflows.get_workflows()
	#print(wf)

	print("Now printing the list of the histories")

	print(histories)

	print("Now Uploading the file")
	input_path = r"/home/vidit/Desktop/Galaxy-Jupyter/Dev_Galaxy_Jupyter/large.fastq.gz"

	uploaded_file = galaxy_instance.tools.upload_file(input_path , history_id  )

	print(uploaded_file)

#jobs = galaxy_instance.jobs.get_jobs()
#print(jobs)

if __name__ == '__main__' :
	main()


